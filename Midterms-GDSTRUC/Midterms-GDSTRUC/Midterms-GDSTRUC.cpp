#include <iostream>
#include <string>
#include <algorithm>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "\nGenerated array: " << endl;
	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
	cout << "\nOrdered: ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";
	cout << endl << endl;


	int choice = 0;
	do
	{
		cout << "What do you want to do?" << endl;
		cout << "1 - Remove element at index" << endl;
		cout << "2 - Search for element" << endl;
		cout << "3 - Expand and generate random values" << endl;
		cin >> choice;

		if (choice == 1)
		{
			int val = 0;
			cout << "Enter index to remove: ";
			cin >> val;

			if (val <= unordered.getSize() && val <= ordered.getSize())
			{
				unordered.remove(val);
				ordered.remove(val);

				cout << "Unordered: ";
				for (int i = 0; i < unordered.getSize(); i++)
					cout << unordered[i] << "   ";
				cout << "\nOrdered: ";
				for (int i = 0; i < ordered.getSize(); i++)
					cout << ordered[i] << "   ";
				cout << endl << endl;
			}
			else
			{
				cout << "Index out of range!" << endl << endl;
				continue;
			}
		}
		else if (choice == 2)
		{
			cout << "\n\nEnter number to search: ";
			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);

			if (result >= 0)
				cout << input << " was found at index " << result << ".\n" << endl;
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n" << endl;
			else
				cout << input << " not found." << endl << endl;
		}
		else if (choice == 3)
		{
			cout << "Input size of expansion: ";
			cin >> size;

			for (int i = 0; i < size; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			cout << "\nGenerated array: " << endl;
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
			cout << endl;
		}

	} while (choice > 0 && choice < 4);

	system("pause");
}

