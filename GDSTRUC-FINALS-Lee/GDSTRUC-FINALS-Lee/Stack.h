#pragma once
#include <string>
#include <iostream>
#include "UnorderedArray.h"
using namespace std;

template <class T>
class Stack
{
	public: 
		Stack(int size)
		{
			stack = new UnorderedArray<T>(size);
		}

		virtual void push(T val)
		{
			stack->push(val);
		}

		void pop()
		{
			stack->pop();
		}
		
		T top()
		{
			if (stack->getSize() == 0)
			{
				return NULL;
			}
			else
			{
				return (*stack)[stack->getSize() - 1];
			}
		}

		void printStack()
		{
			if (stack->getSize() == 0)
			{
				cout << "Stack Elements: " << endl;
				return;
			}
			else
			{
				cout << "Stack Elements: " << endl;
				for (int i = stack->getSize() - 1; i >= 0 ; i--)
				{
					cout << (*stack)[i] << endl;
				}
			
				while (stack->getSize() > 0)
				{
					stack->remove(0);
				}
			}
		}

	private:
		UnorderedArray<T>* stack;
};

