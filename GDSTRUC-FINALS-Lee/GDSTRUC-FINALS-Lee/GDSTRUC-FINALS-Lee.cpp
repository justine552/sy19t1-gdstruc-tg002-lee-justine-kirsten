#include <iostream>
#include <string>
#include <conio.h>
#include "Stack.h"
#include "Queue.h"
using namespace std;

int main()
{
	int size; 
	cout << "Input size: ";
	cin >> size;

	Queue<int> queue = Queue<int>(size);
	Stack<int> stack = Stack<int>(size);
	int choice;
	int num = 0;

	do
	{
		system("cls");
		cout << "1. Push" << endl;
		cout << "2. Pop" << endl;
		cout << "3. Print Elements" << endl;
		cout << "4. Exit" << endl;
		cout << "Enter choice: ";
		cin >> choice;
		switch (choice)
		{ 
		case 1:
			system("cls");
			cout << "Enter element to be pushed: ";
			cin >> num;
			stack.push(num);
			queue.push(num);
			cout << "First Element (Stack): " << stack.top() << endl;
			cout << "First Element (Queue): " << queue.top() << endl;
			_getch();
			break;
		case 2:
			system("cls");
			cout << "You have popped the front elements." << endl;
			stack.pop();
			queue.pop();
			_getch();
			break;
		case 3:
			system("cls");
			stack.printStack();
			queue.printQueue();
			_getch();
			break;
			break;
		case 4:
			exit(0);
			break;
		default:
			system("cls");
			cout << "Input again." << endl;
			_getch();
			break;
		}
	} while (choice != 4);
	return 0;
}

