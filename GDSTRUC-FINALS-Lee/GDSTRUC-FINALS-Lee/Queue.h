#pragma once
#include <iostream>
#include <string>
#include "UnorderedArray.h"
using namespace std;

template <class T>
class Queue
{
	public:
		Queue(int size)
		{
			queue = new UnorderedArray<T>(size);
		}

		virtual void push(T val)
		{
			queue->push(val);
		}

		void pop()
		{
			if (queue->getSize() == 0)
			{
				return;
			}
			else
			{
				queue->remove(0);
			}
		}
		T top()
		{
			if (queue->getSize() == 0)
			{
				return NULL; 
			}
			else
			{
				return (*queue)[0];
			}
		}
		void printQueue()
		{
			if (queue->getSize() == 0)
			{
				cout << "Queue Elements: " << endl;
				return;
			}
			else
			{
				cout << "Queue Elements: " << endl;
				for (int i = 0; i < queue->getSize(); i++)
				{
					cout << (*queue)[i] << endl;
				}
				while (queue->getSize() > 0)
				{
					queue->remove(0);
				}
			}
		}

	private:
		UnorderedArray<T>* queue;
};

