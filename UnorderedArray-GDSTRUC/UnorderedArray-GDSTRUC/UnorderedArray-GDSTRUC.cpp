#include <string>
#include <iostream>
#include "UnorderedArray.h"

using namespace std;

int main()
{
	UnorderedArray<int> grades(5);

	for (int i = 0; i < 10; i++)
	{
		grades.push(i);
	}

	cout << " Initial values: \n";
	for (int i = 0; i < grades.getSize(); i++)
	{
		cout << grades[i] << endl;
	}

	cout << "\n\n Remove index 1 \n";
	grades.remove(1);

	cout << "\n\n After removing index 1: \n";
	for (int i = 0; i < grades.getSize(); i++)
	{
		cout << grades[i] << endl;
	}

	cout << "\n\n Linear Search: \n ";
	cout << grades.search(5);
	cout << endl;

	system("pause");
}