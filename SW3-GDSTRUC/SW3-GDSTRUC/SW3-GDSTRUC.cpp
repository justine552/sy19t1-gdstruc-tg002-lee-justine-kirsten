#include <iostream>
#include <string>
#include "GuildMember.h"
#include <vector>
using namespace std;

GuildMember* createGuildMember()
{
	return new GuildMember;
}

void inputNames(vector<GuildMember> &members)
{
	string memberName;
	string guildName;
	int size;

	cout << "Input name of guild: " << endl;
	cin >> guildName;
	cout << "Input number of members: " << endl;
	cin >> size;

	for (int i = 0; i < size; i++)
	{
		cout << "Input member name: " << endl;
		cin >> memberName;

		GuildMember newMember(memberName);
		members.push_back(newMember);
		cout << endl;
	}
}

int inputPlayerAction()
{
	int choice = 0;
	do
	{
		cout << "What do you want to do?" << endl;
		cout << "[1] Print members" << endl;
		cout << "[2] Rename a member" << endl;
		cout << "[3] Add a member" << endl;
		cout << "[4] Delete a member" << endl;
		cout << "[5] Exit" << endl;
		cout << "Choice: " << endl;
		cin >> choice;
		system("cls");
	} while (choice <= 0  || choice >= 6 );
	return choice;
}

void printMembers(vector<GuildMember> &members)
{
	int size = members.size();

	cout << "Members: " << endl;
	for (int i = 0; i < size; i++)
	{
		cout << "[" << i + 1 << "]" << members[i].getName() << endl;
	}
	system("pause");
}

void addMember(vector<GuildMember>& members)
{
	string memberName;
	cout << "Input member name: " << endl;
	cin >> memberName;

	GuildMember newMember(memberName);
	members.push_back(newMember);
	system("pause");
}

void deleteMember(vector<GuildMember>& members)
{
	int memberNum = 0;
	cin >> memberNum;
	members.erase(members.begin() + memberNum - 1);
}

void renameMember(vector<GuildMember>& members)
{
	int choice = 0;
	string name;
	cout << "Which number do you want to rename?" << endl;
	cin >> choice;
	cout << "What name do you want?" << endl;
	cin >> name;
	
	members[choice - 1] = name;
}

void processPlayerAction(int choice, vector<GuildMember> &members)
{
	string name;
	switch (choice)
	{
	case 1:
		printMembers(members);
		break;
	case 2:
		printMembers(members);
		renameMember(members);
		break;
	case 3:
		addMember(members);
		break;
	case 4:
		printMembers(members);
		deleteMember(members);
		break;
	case 5: 
		break;
	default:
		break;
	}

}


int main()
{
	string name;

	GuildMember* guildMember = createGuildMember();

	vector<GuildMember> members;
	int playerChoice = 0;
	inputNames(members);
	printMembers(members);
	system("cls");
	while (playerChoice != 5)
	{
		playerChoice = inputPlayerAction();
		processPlayerAction(playerChoice, members);
	}


	system("pause");
}