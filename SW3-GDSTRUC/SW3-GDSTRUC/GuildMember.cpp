#include "GuildMember.h"

GuildMember::GuildMember()
{
}

GuildMember::GuildMember(string name)
{
	mName = name;
}

GuildMember::~GuildMember()
{
}

string GuildMember::getName()
{
	return mName;
}

void GuildMember::setName(string name)
{
	mName = name;
}

