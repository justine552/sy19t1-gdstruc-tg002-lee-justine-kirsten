#pragma once
#include <string>
#include <iostream>
#include <vector>
using namespace std;

class GuildMember 
{
	public:
		GuildMember();
		GuildMember(string name);
		~GuildMember();

		string getName();
		
		void setName(string name);

	private:
		string mName;
};

