#include <iostream>
#include <string>
using namespace std;

int computeSumDigits(int n) //1 reference: https://www.geeksforgeeks.org/sum-digit-number-using-recursion/
{
	if (n == 0)
		return 0;

	return (n % 10 + computeSumDigits(n / 10));
}

void printSumDigits()
{
	int n;
	cout << "Input numbers: " << endl;
	cin >> n;
	cout << "Sum: " << computeSumDigits(n) << endl;
	system("pause");
	system("cls");
}

int fibonacciNum(int n) //2 reference: https://javascript.info/task/fibonacci-numbers
{
	if (n < 0)
		return NULL;
	if (n == 0)
		return 0;

	if (n == 1)
		return 1;

	return fibonacciNum(n - 1) + fibonacciNum(n - 2);
}

void printFibonacciNum()
{
	int n;
	cout << "2. Input nth value for Fibonacci: ";
	cin >> n;
	cout << fibonacciNum(n) << endl;
	system("pause");
	system("cls");
}

int primeCheck(int num, int i) //3 reference: https://www.sanfoundry.com/c-program-prime-number-using-recursion/
{
	if (num <= 1)
		return 0;
	if (i == 1)
		return 1;

	else
		if (num % i == 0)
			return 0;
	
		else
			return primeCheck(num, i - 1);
}

void printPrimeCheck()
{
	int num, check;
	cout << "Enter a number: " << endl;
	cin >> num;
	check = primeCheck(num, num / 2);

	if (check == 1)
	{
		cout << num << " is a prime number" << endl;
	}
	else
	{
		cout << num << " is not a prime number" << endl;
	}
	system("pause");
	system("cls");
}

void inputChoice(int choice)
{
	switch (choice)
	{
	case 1:
		printSumDigits();
		break;
	case 2:
		printFibonacciNum();
		break;
	case 3:
		printPrimeCheck();
		break;
	case 4:
		break;
	default:
		break;
	}
}

int main()
{
	int choice = 0;
	while (choice != 4)
	{
		cout << "1. Sum of Digits" << endl;
		cout << "2. Fibonacci" << endl;
		cout << "3. Prime Number Checker" << endl;
		cout << "4. Exit" << endl;
		cin >> choice;
		system("cls");
		inputChoice(choice);
		continue;
	}
	system("pause");
	return 0;
}